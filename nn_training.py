from sklearn.model_selection import train_test_split
import glob
import h5py
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D, MaxPooling1D, Flatten, Dropout, AveragePooling1D, concatenate
import tensorflow as tf
from sklearn.preprocessing import StandardScaler
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import math 
import re
from pylab import *
import matplotlib
matplotlib.use('agg')

tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.threading.set_inter_op_parallelism_threads(1)


np.set_printoptions(suppress=True)


files = glob.glob('/user/1002/zfasnach/Subset_Rads/TROPOMI_OC_2nmSpacing_1nmFWHM/2020/12/05/*')

start = True

for filename in files[0:1]:
    #Skipping Dec 9 to use as validation
    if '2020m1209' in filename:
        continue


    
    orb = re.findall('-o[0-9]{5}',filename)[0]
    year = int(re.findall('202[0-1]m[0-9]{4}',filename)[0][0:4])
    month = int(re.findall('202[0-1]m[0-9]{4}',filename)[0][-4:-2])
    day = int(re.findall('202[0-1]m[0-9]{4}',filename)[0][-2:])


    #Reading pre-processed training file with smoothed radiances and other ancillary data
    f = h5py.File(filename,'r')

    ai = f['UV_AI'][:].flatten()
    waves = np.hstack((f['B3_Waves'],f['B4_Waves']))
    rad = f['NormalizedRadiance'][:].reshape(len(ai),len(waves))
    x,y = f['UV_AI'][:].shape
    sza = np.array(f['SZA'][:]).flatten()

    #Land water classification co-located from MODIS as TROPOMI land/water classification seems incorrect 
    land_water = np.array(f['Land_Water'][:].reshape(x,y)[:],dtype=np.int).flatten()
    sza[sza ==-999]  =np.nan
    ecf = np.array(f['ECF'][:]).flatten()
    ecf[ecf == -999] = np.nan

    lat = np.array(f['lat'][:]).flatten()
    lon = np.array(f['lon'][:]).flatten()
    vza = np.array(f['VZA'][:]).flatten()
    raa = np.array(f['RAA'][:]).flatten()
    
    f.close()

    #Grabbing TROPOMI Snow/Ice information to remove any snow/ice from training
    snow_file = glob.glob('/tis/local_TropOMI/20000/S5PB4SNOICE/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)+'/*'+orb+'*h5')[0]
    f = h5py.File(snow_file,'r')
    snow = f['/SnowIceFlag'][:].flatten()
    f.close()

    #Grabbing TROPOMI/MODIS Co-location ocean fields
    #Gap filling daily MODIS measurements with 8-day MODIS measurements to get data under clouds 
    mod_file = glob.glob('/tis/local_TropOMI/20000/S5PMODOC/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)+'/*'+orb+'*h5')[0]
    f = h5py.File(mod_file,'r')

    aq_daily_chl = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/AquaDailyChlMean'][:]).flatten()
    aq_8day_chl = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/Aqua8DayChlMean'][:]).flatten()
    aq_daily_chl[aq_daily_chl == -2**100]  = np.nan
    aq_8day_chl[aq_8day_chl == -2**100]  = np.nan

    aq_daily_rrs412 = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/AquaDailyRRS412Mean'][:]).flatten()
    aq_8day_rrs412 = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/Aqua8DayRRS412Mean'][:]).flatten()
    aq_daily_rrs412[aq_daily_rrs412 == -2**100]  = np.nan
    aq_8day_rrs412[aq_8day_rrs412 == -2**100]  = np.nan

    aq_daily_rrs443 = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/AquaDailyRRS443Mean'][:]).flatten()
    aq_8day_rrs443 = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/Aqua8DayRRS443Mean'][:]).flatten()
    aq_daily_rrs443[aq_daily_rrs443 == -2**100]  = np.nan
    aq_8day_rrs443[aq_8day_rrs443 == -2**100]  = np.nan

    aq_daily_rrs488 = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/AquaDailyRRS488Mean'][:]).flatten()
    aq_8day_rrs488 = np.array(f['/HDFEOS/SWATHS/OceanColor/Data Fields/Aqua8DayRRS488Mean'][:]).flatten()
    aq_daily_rrs488[aq_daily_rrs488 == -2**100]  = np.nan
    aq_8day_rrs488[aq_8day_rrs488 == -2**100]  = np.nan
    f.close()
    
    chl = np.array(aq_daily_chl)
    chl[np.isnan(chl)] = aq_8day_chl[np.isnan(chl)]

    rrs412 = np.array(aq_daily_rrs412)
    rrs412[np.isnan(rrs412)] = aq_8day_rrs412[np.isnan(rrs412)]

    rrs443 = np.array(aq_daily_rrs443)
    rrs443[np.isnan(rrs443)] = aq_8day_rrs443[np.isnan(rrs443)]

    rrs488 = np.array(aq_daily_rrs488)
    rrs488[np.isnan(rrs488)] = aq_8day_rrs488[np.isnan(rrs488)]


    #Removing bad data with high SZA, bad radiances, snow/ice, overcast clouds, heavy aerosol loading, and land 
    good_rad =  (~np.isnan(rrs412)) & (~np.isnan(chl)) & (~np.isnan(np.min(rad,axis=1))) & (sza < 70) & (np.nanmin(rad,axis=1) > 0) & (~np.isnan(ecf)) &(~np.isnan(ai))    &  (snow==104) &(chl > 0) & (lat > -65)& (ecf < 0.5)  &((land_water == 0) | (land_water > 2) )  & (np.abs(ai) < 2.5) 


    chl = chl[good_rad]
    lat = lat[good_rad]

    rrs412 = rrs412[good_rad]
    rrs443 = rrs443[good_rad]
    rrs488 = rrs488[good_rad]
    vza = vza[good_rad]    
    raa = raa[good_rad]    
    sza = sza[good_rad]
    rad = rad[good_rad,:]

    #Storing training data from multiple orbits/days 
    if start:
        tot_chl = np.array(chl)
        tot_rrs412 = np.array(rrs412)
        tot_rrs443 = np.array(rrs443)
        tot_rrs488 = np.array(rrs488)
        tot_sza = np.array(sza)
        tot_vza = np.array(vza)
        tot_raa = np.array(raa)
        tot_rad = np.array(rad)
        start = False
    else:

        tot_chl = np.hstack((tot_chl,chl))
        tot_rrs412 = np.hstack((tot_rrs412,rrs412))
        tot_rrs443 = np.hstack((tot_rrs443,rrs443))
        tot_rrs488 = np.hstack((tot_rrs488,rrs488))
        tot_sza = np.hstack((tot_sza,sza))
        tot_vza = np.hstack((tot_vza,vza))
        tot_raa = np.hstack((tot_raa,raa))
        tot_rad = np.vstack((tot_rad,rad))





#Training on log of radiances 
rad = np.log(tot_rad)
    
#Perform PCA decomposition. Here radiance is rad(n_sample,n_lambda)
covmatrix = np.dot(rad.T ,rad)
_, pca_coeff = np.linalg.eigh(covmatrix)
pca_coeff = pca_coeff[:,::-1]


pca_proj = np.swapaxes(np.dot(pca_coeff.T,rad.T),0,1)

n_pc = 40

inputs = np.array(pca_proj[:,0:n_pc])
inputs = np.hstack((inputs,np.cos(np.radians(tot_sza.reshape(-1,1))),np.cos(np.radians(tot_vza.reshape(-1,1))),np.cos(np.radians(tot_raa.reshape(-1,1)))))


#Scaling inputs and outputs to 0->1 for training
sc_in = MinMaxScaler(feature_range=(0.0,1))
inputs = sc_in.fit_transform(inputs)

sc_out = MinMaxScaler(feature_range=(0.0,1))
outputs = np.swapaxes(np.vstack((np.log(tot_chl),tot_rrs412,tot_rrs443,tot_rrs488)),0,1)     
outputs = sc_out.fit_transform(outputs)

#Storing PCA coefficients and the scale factors for training inputs/outputs
input_scale = sc_in.scale_
input_min = sc_in.data_min_

output_scale = sc_out.scale_
output_min = sc_out.data_min_    
    
f = h5py.File('TROPOMI_NN/TROPOMI_Scaling_OC.h5','w')
f.create_dataset('Input_Scale',data=input_scale)
f.create_dataset('Input_Min',data=input_min)
f.create_dataset('PCA_Coeff',data=pca_coeff)
f.create_dataset('Output_Scale',data=output_scale)
f.create_dataset('Output_Min',data=output_min)
f.close()



#Splitting into training and test data
inputs,inputs_test,outputs,outputs_test = train_test_split(inputs,outputs,test_size=0.8)

#Building the NN structure
nn_inp = tf.keras.Input(shape=(1,len(inputs[0,:])))
out = tf.keras.layers.Dense(64, activation='sigmoid')(nn_inp)
out = tf.keras.layers.Dropout(0.05)(out)
out = tf.keras.layers.Dense(64, activation='sigmoid')(out)
out = tf.keras.layers.Dropout(0.05)(out)
out = tf.keras.layers.Dense(64, activation='sigmoid')(out)
out = tf.keras.layers.Dropout(0.05)(out)
out = tf.keras.layers.Dense(4,activation='sigmoid')(out)

x, y = inputs.shape
a,b = outputs.shape
    
    
model = tf.keras.Model(nn_inp, out, name="OceanColor")
opt = tf.keras.optimizers.Adam(learning_rate=0.001)
model.compile(loss='mse', optimizer=opt, metrics=['mae','mse','acc','mape'])
history = model.fit(inputs.reshape(x,1,y), outputs.reshape(a,1,b), epochs=500, batch_size=124,validation_split=0.5)

#Storing the model itself as well as a separate file with the model weights. The ACPS doesn't have tensorflow so right now to run on the ACPS I use
#the model weights and perform matrix multiplication to calculate NN predictions 
model.save("TROPOMI_NN/TROPOMI_model")
model.save_weights("TROPOMI_NN/TROPOMI_model_Weights.h5",'w')

#Plotting the training loss and validation loss to see if model is overfitting 
plt.plot(history.history['loss'],color='b')
plt.plot(history.history['val_loss'],color='r')
plt.savefig('Loss_All_2nmSpacing_1nmFWHM_SIGMOID_PC40_Dec5Days_LogRad_80PercTrain_64Nodes_Dropout_ChlOnly_ShallowWater.png')
plt.clf()
plt.close()
    
tf.keras.backend.clear_session()






