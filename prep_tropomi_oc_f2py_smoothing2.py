from pathlib import Path
from scipy.interpolate import interpn
import time
import smoothing
import os.path
from scipy.interpolate import RegularGridInterpolator
import datetime
import h5py
import numpy as np
import glob
import re 
import math 
from scipy.interpolate import interp1d
import sys
from netCDF4 import Dataset 
from scipy import interpolate
import time 
import scipy 


def do_kdtree(combined_x_y_arrays,points):
    mytree = scipy.spatial.cKDTree(combined_x_y_arrays)
    dist, indexes = mytree.query(points)
    return indexes

def read_nc(filename):
        try:
                return Dataset(filename,'r')
        except:
                print('NOT READ', filename)
                time.sleep(5)
                return read_nc(filename)

def read_h5(filename):
        try:
                return h5py.File(filename,'r')
        except:
                time.sleep(5)
                return read_h5(filename)

start = True
start_wave = True

month_st = int(sys.argv[1])

#Grabbing MODIS Land/Water classification file. TROPOMI Land/water flags are not good 
f = h5py.File('/app/OMULNDTYP-0.0.19/tbl/Regrid_WaterMask.h5','r')
water_mask = f['WaterMask'][:]
x, y = water_mask.shape

lat_bins = np.linspace(-90,90,x+1)[::-1]
water_lat = (lat_bins[0:-1]+lat_bins[1:])/2.

lon_bins = np.linspace(-180,180,y+1)
water_lon = (lon_bins[0:-1]+lon_bins[1:])/2.


for year in range(2020,2021):
    for month in range(month_st,month_st+1):
        for day in [3,4,6,7,8]:
            
            print(year,month,day)
            doy = float(datetime.datetime(year,month,day).strftime('%j'))

            #Grabbing TROPOMI L1b Radiance file
            files = glob.glob('/tis/GES_DISC/1/S5P_L1B_RA_BD3_HiR/'+str(year)+'/'+str(int(doy)).zfill(3)+'/S5P_OFFL*.nc')[::-1]

            start = True
            for filename in files:
                print(filename)
                orb = re.findall('_[0-9]{5}_',filename)[0]
            
                f = read_nc(filename)

                b3_rad = f['/BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance'][0,:,:]
                ql = f['/BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality'][0,:,:]
                b3_rad[ql != 0] = np.nan


                b3_wave = f['/BAND3_RADIANCE/STANDARD_MODE/INSTRUMENT/nominal_wavelength'][0,:,:]
                b3_rad=np.ma.filled(b3_rad,float('nan'))
                b3_wave=np.ma.filled(b3_wave,float('nan'))
                            
                sza = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/solar_zenith_angle'][0,:,:]
                gpqf =  f['/BAND3_RADIANCE/STANDARD_MODE/OBSERVATIONS/ground_pixel_quality'][0,:,:]
                sza[(gpqf != 0) & (gpqf != 2)] = np.nan
                saa = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/solar_azimuth_angle'][0,:,:]
                vaa = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/viewing_azimuth_angle'][0,:,:]
                vza = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/viewing_zenith_angle'][0,:,:]

                
                lat = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/latitude'][0,:,:]
                lon = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/longitude'][0,:,:]


                #Co-locating MODIS Land/Water classification to TROPOMI FoV
                tmp_lat = np.array(lat)
                tmp_lat[tmp_lat > np.nanmax(water_lat)] = np.nanmax(water_lat) - 0.05
                tmp_lat[tmp_lat < np.nanmin(water_lat)] = np.nanmin(water_lat) + 0.05

                tmp_lon = np.array(lon)
                tmp_lon[tmp_lon >= np.nanmax(water_lon)] = np.nanmax(water_lon) - 0.01
                tmp_lon[tmp_lon <= np.nanmin(water_lon)] = np.nanmin(water_lon) + 0.01

                tropomi_water_mask = interpn((water_lat[::-1],water_lon),water_mask[::-1,:] ,(tmp_lat.flatten(),tmp_lon.flatten()),method='nearest')

                
                lon_corns = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/longitude_bounds'][0,:,:]
                lat_corns = f['/BAND3_RADIANCE/STANDARD_MODE/GEODATA/latitude_bounds'][0,:,:]
                
                #Calculating RAA for TROPOMI
                raa = np.array(saa,dtype=np.float)-np.array(vaa,dtype=np.float)
                raa[raa > 360] = raa[raa > 360] - 360
                raa[raa < 00] = raa[raa < 0] + 360
                raa = np.abs(raa-180)

                #Calculating row numbers for TROPOMI
                row_arr = np.repeat(np.arange(450)[np.newaxis,:],len(lat[:,0]),axis=0)

                f.close()

                #Reading in corresponding TROPOMI Band 4 files
                b4_file = glob.glob('/tis/GES_DISC/1/S5P_L1B_RA_BD4_HiR/'+str(year)+'/'+str(int(doy)).zfill(3)+'/S5P_OFFL*'+orb+'*.nc')
                print(b4_file)
                if len(b4_file) == 0:
                    print('NO B4')
                    continue
                f = read_nc(b4_file[0])
                
                b4_rad = f['/BAND4_RADIANCE/STANDARD_MODE/OBSERVATIONS/radiance'][0,:,:]
                ql = f['/BAND4_RADIANCE/STANDARD_MODE/OBSERVATIONS/spectral_channel_quality'][0,:,:]
                b4_rad[ql != 0] = np.nan
                
                b4_wave = f['/BAND4_RADIANCE/STANDARD_MODE/INSTRUMENT/nominal_wavelength'][0,:,:]

                f.close()

                b4_rad=np.ma.filled(b4_rad,float('nan'))
                b4_wave=np.ma.filled(b4_wave,float('nan'))
                

                #Reading in dynamic TROPOMI irradiance file
                irr_file = glob.glob('/tis/GES_DISC/1/S5P_L1B_IR_UVN/'+str(year)+'/'+str(int(doy)).zfill(3)+'/S5P_*.nc')
                print(irr_file)
                if len(irr_file) ==0:
                    print('NO L1BIRR')
                    continue
                else:
                    
                    f = read_nc(irr_file[0])
                    b3_irr = f['/BAND3_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance'][0,0,:,:]
                    b3_irr_wave = f['/BAND3_IRRADIANCE/STANDARD_MODE/INSTRUMENT/nominal_wavelength'][0,:,:]
                    b4_irr = f['/BAND4_IRRADIANCE/STANDARD_MODE/OBSERVATIONS/irradiance'][0,0,:,:]
                    b4_irr_wave = f['/BAND4_IRRADIANCE/STANDARD_MODE/INSTRUMENT/nominal_wavelength'][0,:,:]

                f.close()

                
                b3_irr_interp = np.zeros_like(b3_irr)
                b4_irr_interp = np.zeros_like(b4_irr)
                
                b3_irr=np.ma.filled(b3_irr,float('nan'))
                b3_irr_wave=np.ma.filled(b3_irr_wave,float('nan'))
                b4_irr=np.ma.filled(b4_irr,float('nan'))
                b4_irr_wave=np.ma.filled(b4_irr_wave,float('nan'))

                #Interpolating TROPOMI irradiances to the B3 and B4 wavelength grids

                for row in range(450):
                    f = interp1d(b3_irr_wave[row,:],b3_irr[row,:],bounds_error=False,fill_value=np.nan)
                    b3_irr_interp[row,:] = f(b3_wave[row,:])
                    
                    f = interp1d(b4_irr_wave[row,:],b4_irr[row,:],bounds_error=False,fill_value=np.nan)
                    b4_irr_interp[row,:] = f(b4_wave[row,:])


                #Grabbing ancillary information including aerosol and clouds 
                aerosol_file = glob.glob('/tis/local_TropOMI/20000/TROPOMAER/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)+'/TROPOMI-*-o'+str(orb).replace('_','')+'*_v01*')
                if len(aerosol_file) == 0:
                        print('NO Aerosol')
                        continue

                f = h5py.File(aerosol_file[0],'r')
                try:
                        uv_ai  = f['/SCIDATA/UVAerosolIndex'][:]
                except:
                        uv_ai  = f['/ScienceData/UVAerosolIndex'][:]
                f.close()


                
                cld_file = glob.glob('/tis/local_TropOMI/20000/S5PNO2CLD/'+str(year)+'/'+str(int(month)).zfill(2)+'/'+str(int(day)).zfill(2)+'/TROPOMI*-o'+orb.replace('_','')+'*.h5')[0]
                f = h5py.File(cld_file,'r')

                ecf = f['/EffectiveCloudFraction_GSFC'][:]
                f.close()
                print('cld and aerosol')
                
                if np.shape(ecf) != np.shape(sza):
                    ecf = np.zeros_like((sza)) + np.nan
                    uv_ai = np.zeros_like((sza)) + np.nan

                
                #Calculating normalized radiances at native TROPOMI B3 and B4 wavelength grids
                try:
                        b3_norm_rad = np.array(b3_rad)/np.repeat(b3_irr[np.newaxis,:,:],len(sza[:,0]),axis=0)
                        b4_norm_rad = np.array(b4_rad)/np.repeat(b4_irr[np.newaxis,:,:],len(sza[:,0]),axis=0)


                except:
                        print('BAD B4 Norm Rad',filename)
                        b3_norm_rad = np.zeros_like((b3_rad)) + np.nan
                        b4_norm_rad = np.zeros_like((b4_rad)) + np.nan

                #Defining the regulare spectral grid to smooth TROPOMI radiances
                b3_wave_out = np.arange(320,395,2)
                b4_wave_out = np.arange(410,495,2)


                b3_norm_interp = np.zeros((len(sza[:,0]),450,len(b3_wave_out))) + np.nan
                b4_norm_interp = np.zeros((len(sza[:,0]),450,len(b4_wave_out))) + np.nan
                

                #Using fortran triangular smoothing library.
                #smoothed_radiances = smoothing.smoothing.triangle_2d(fwhm,original_data,original_wavelength,smoothed_wavelength)
                
                for row in range(450):
 
                    b3_norm_interp[:,row,:] = smoothing.smoothing.triangle_2d(1,b3_norm_rad[:,row,:],b3_wave[row],b3_wave_out)
                    b4_norm_interp[:,row,:] = smoothing.smoothing.triangle_2d(1,b4_norm_rad[:,row,:],b4_wave[row],b4_wave_out)


                b3_norm_interp[b3_norm_interp == 0.] = np.nan
                b4_norm_interp[b4_norm_interp == 0.] = np.nan

                b3_norm_rad = b3_norm_interp.reshape(len(sza[:,0])*len(sza[0,:]),len(b3_wave_out))
                b4_norm_rad = b4_norm_interp.reshape(len(sza[:,0])*len(sza[0,:]),len(b4_wave_out))

                                
                tropomi_uv_norm_rad = np.hstack((b3_norm_rad,b4_norm_rad))
                nsample, nwave = tropomi_uv_norm_rad.shape


                nline,nrow,ncorner = lon_corns.shape

                #Saving the smoothed radiances to an h5 file
                tropomi_path = '/user/1002/zfasnach/Subset_Rads/TROPOMI_OC_2nmSpacing_1nmFWHM/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)
                Path(tropomi_path).mkdir(parents=True, exist_ok=True)
                
                f = h5py.File(tropomi_path+'/TROPOMI_'+str(year)+'m'+str(month).zfill(2)+str(day).zfill(2)+'-o'+orb.replace('_','')+'.h5','w')
                f.create_dataset('B3_Waves',data=b3_wave_out)
                f.create_dataset('B4_Waves',data=b4_wave_out)
                f.create_dataset('LatitudeCorners',data=lat_corns.reshape(nline,nrow,ncorner))
                f.create_dataset('LongitudeCorners',data=lon_corns.reshape(nline,nrow,ncorner))
                f.create_dataset('SZA',data=sza,dtype=np.float32)
                f.create_dataset('UV_AI',data=uv_ai,dtype=np.float32)
                f.create_dataset('Land_Water',data=tropomi_water_mask,dtype=np.float32)
                f.create_dataset('VZA',data=vza,dtype=np.float32)
                f.create_dataset('RAA',data=raa,dtype=np.float32)
                f.create_dataset('Row',data=row_arr,dtype=np.float32)
                f.create_dataset('lat',data=lat,dtype=np.float32)
                f.create_dataset('lon',data=lon,dtype=np.float32)
                f.create_dataset('ECF',data=ecf,dtype=np.float32)
                f.create_dataset('NormalizedRadiance',data=tropomi_uv_norm_rad.reshape(nline,nrow,nwave),dtype=np.float32)
                f.close()
                


